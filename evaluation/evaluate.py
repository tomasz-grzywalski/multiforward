import sys
import os

import h5py
import yaml
import numpy as np
import cPickle as pickle

from reco import testsdr
from stoi2 import stoi as teststoi
from stoi2 import metrics
import librosa
import soundfile as sf

import vqmetrics

DUMP_PATH = "./eval_dump/"

def get_default_pars():
    pars = {'properties_file': '../properties.yaml'}
    return pars

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "Please provide input file name (x_pred.pkl)"
        exit(-1)

    predictions = pickle.load(open(sys.argv[1], "rb"))

    if not os.path.exists(DUMP_PATH):
        os.makedirs(DUMP_PATH)

    maxv = np.iinfo(np.int16).max
    sdr_old = 0.0
    stoi = 0.0
    sdr = 0.0
    sir = 0.0
    sar = 0.0
    pesq = 0.0
    for idx in range(len(predictions)):
        preds, original_data = predictions[idx]
        x_mag, x_phase, y_mag, y_phase, n_mag, n_phase, _ = original_data

        x_mag_pred = y_mag.copy()
        x_mag_pred[:256] = np.abs(preds)

        x_phase_pred = y_phase.copy()
        x_phase_pred[:256] = np.angle(preds)


        sdr_old += testsdr(x_mag_pred, x_phase_pred, x_mag, x_phase)

        complex_noisy = y_mag*np.exp(1j*y_phase)
        noisy_wave = librosa.core.istft(complex_noisy, hop_length=80, win_length=200, window='hann')
        noisy_wave /= 1.1*np.max(np.abs(noisy_wave))

        filename_noisy = os.path.join(DUMP_PATH, "{}_0_noisy.wav".format(str(idx+1).zfill(3)))
        sf.write(filename_noisy, (noisy_wave*maxv).astype(np.int16), 8000)

        complex_clean = x_mag_pred*np.exp(1j*x_phase_pred)
        clean_wave = librosa.core.istft(complex_clean, hop_length=80, win_length=200, window='hann')
        clean_wave /= 1.1*np.max(np.abs(clean_wave))

        filename_clean = os.path.join(DUMP_PATH, "{}_1_clean.wav".format(str(idx+1).zfill(3)))
        sf.write(filename_clean, (clean_wave*maxv).astype(np.int16), 8000)

        complex_original = x_mag*np.exp(1j*x_phase)
        original_wave = librosa.core.istft(complex_original, hop_length=80, win_length=200, window='hann')
        original_wave /= 1.1*np.max(np.abs(original_wave))

        filename_orig = os.path.join(DUMP_PATH, "{}_2_original.wav".format(str(idx+1).zfill(3)))
        sf.write(filename_orig, (original_wave*maxv).astype(np.int16), 8000)

        complex_noise = n_mag*np.exp(1j*n_phase)
        noise_wave = librosa.core.istft(complex_noise, hop_length=80, win_length=200, window='hann')

        stoi += teststoi(clean_wave, original_wave, 8000)

        pesq_idx = vqmetrics.pesq(filename_orig, filename_clean, 8000, 'pesq')[0]

        sd, si, sa = metrics(original_wave, clean_wave, noise_wave)
        sdr += sd
        sir += si
        sar += sa
        pesq += pesq_idx

    print "SDR:       {}".format(sdr_old/len(predictions))
    print "SDR (alt): {}".format(sdr/len(predictions))
    print "SIR:       {}".format(sir/len(predictions))
    print "SAR:       {}".format(sar/len(predictions))
    print "STOI:      {}".format(stoi/len(predictions))
    print "PESQ:      {}".format(pesq/len(predictions))
