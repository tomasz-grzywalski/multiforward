import os
import numpy as np
import random
import h5py
import yaml
import sys
import math
from multiprocessing import Process, JoinableQueue
import librosa
from scipy.signal import convolve, medfilt
from scipy.ndimage.morphology import binary_dilation

from evaluation.reco import testsdr

MAX_VALTEST_LENGTH = 3045
INITIAL_STATS_SAMPLE_SIZE = 200
SAMPLE_LENGTH_STEP_SIZE = 4
BABBLE_SPEC_SCALING = 40.0
SHOPMALL_SPEC_SCALING = 6.0

class Dataset:
    def __init__(self, data_root, train_batch_size, network, snr=0.0, test_only=False, test_shopmall=False):
        self.train_batch_size = train_batch_size
        self.network = network
        self.snr = snr
        self.test_only = test_only
        self.test_shopmall = test_shopmall
        self.spec_scaling = SHOPMALL_SPEC_SCALING if test_shopmall else BABBLE_SPEC_SCALING

        self.read_data(data_root)

        if not test_only:
            self.print_data_stats(mode='TRAIN_DETERMINISTIC')
            self.print_data_stats(mode='VAL')
        self.print_data_stats(mode='TEST')
        sys.stdout.flush()

    def get_spec_scaling(self):
        return self.spec_scaling

    def remove_empty_samples(self, sample):
        idx_to_remove = list()
        for idx in range(sample.shape[0]):
            if not self.loudness_check(sample[idx]):
                idx_to_remove.append(idx)
        return np.delete(sample, idx_to_remove, axis=0)

    def loudness_check(self, wave):
        loudness = wave**2

        ave_filter = np.ones((500), dtype=np.float32)
        loudness = convolve(loudness, ave_filter, mode='same')/1000

        loudness[np.where(loudness < 1e-12)] = 1e-12
        loudness = np.log10(loudness)

        min_loudness = 100.0
        for idx in range(5):
            min_loudness = min(min_loudness, np.mean(loudness[idx*16000:(idx+1)*16000]))
        return min_loudness > -5.0

    def get_train_noise(self, data_root):
        noise_root = os.path.join(data_root, 'Robustness', 'hacked', 'MP3s')

        train_noise = list()
        all_recs = 0
        all_samples = 0
        total_recs = 0
        total_samples = 0
        for r, d, f in os.walk(noise_root):
            for file in f:
                if file.endswith(".npz"):
                    noise_sample = np.array(())
                    if not self.test_only:
                        noise_sample = np.load(os.path.join(r, file))['data']
                        if noise_sample.shape[0] > 0:
                            all_recs += 1
                            all_samples += noise_sample.shape[0]
                        noise_sample = self.remove_empty_samples(noise_sample)
                        if noise_sample.shape[0] > 0:
                            total_recs += 1
                            total_samples += noise_sample.shape[0]
                    train_noise.append(noise_sample)

                if len(train_noise) % 1000 == 0:
                    print "Loaded {} train noise recordings".format(len(train_noise))

        print "Loaded {} train noise recordings.".format(len(train_noise))
        print " - All recs      {}, all samples      {}".format(all_recs, all_samples)
        print " - Filtered recs {}, filtered samples {}".format(total_recs, total_samples)
        return np.concatenate(train_noise)

    def read_data(self, data_root):
        self.stft_pars = yaml.load(open(os.path.join(data_root, 'WSJ', 'stft_pars.yaml'), 'r'))
        self.train_noise = self.get_train_noise(data_root)

        dummy = range(7690)
        random.shuffle(dummy)

        np.random.shuffle(self.train_noise)
        if self.test_shopmall:
            self.test_noise = self.get_shop_mall_test_noise(data_root)
        else:
            self.test_noise, _ = librosa.load(os.path.join(data_root, 'WSJ', 'noise_tst.wav'), sr=self.stft_pars['sr'])

        self.train_data = self.read_hdf5(os.path.join(data_root, 'WSJ', 'train_si84.hdf5'))
        random.shuffle(self.train_data)
        self.val_data = self.train_data[:len(self.train_data)/10]
        del self.train_data[:len(self.train_data)/10]

        self.test_data = self.read_hdf5(os.path.join(data_root, 'WSJ', 'test_si84.hdf5'))
        random.shuffle(self.test_data)

    def get_shop_mall_test_noise(self, data_root):
        data_root = os.path.join(data_root, "ShopMall")
        startsfile = os.path.join(data_root, "starts_shopping_mall_tst.txt")
        test_files = open(startsfile, "rt").readlines()
        test_files = [f.strip().split()[1] for f in test_files]
        test_files = [os.path.join(data_root, "shopping_mall", f) for f in test_files]
        test_noises = [librosa.load(f, sr=self.stft_pars['sr'])[0] for f in test_files]
        return np.concatenate(test_noises)

    def read_hdf5(self, filename):
        items = list()
        with h5py.File(filename, 'r') as f:
            n_items = len(f.keys())
            for mix_idx in range(n_items):
                uttid = 'utt_' + str(mix_idx)
                items.append(f[uttid][...])

        return items

    def get_data(self, x, n, snr):
        item=dict()

        rms_x = np.sqrt(np.mean(x**2.0))
        rms_n = np.sqrt(np.mean(n**2.0))

        gx = 10.0**(snr/20.0)*rms_n/rms_x
 
        x *= gx
        y = x + n

        x_complex = librosa.core.stft(x,
                hop_length = self.stft_pars['hop_length'],
                win_length = self.stft_pars['win_length'],
                window = self.stft_pars['window'],
                n_fft = self.stft_pars['n_fft'])
        correct_length = x_complex.shape[1] - x_complex.shape[1] % SAMPLE_LENGTH_STEP_SIZE
        x_complex = x_complex[:, :correct_length]

        n_complex = librosa.core.stft(n,
                hop_length = self.stft_pars['hop_length'],
                win_length = self.stft_pars['win_length'],
                window = self.stft_pars['window'],
                n_fft = self.stft_pars['n_fft'])
        n_complex = n_complex[:, :correct_length]

        y_complex = librosa.core.stft(y,
                hop_length = self.stft_pars['hop_length'],
                win_length = self.stft_pars['win_length'],
                window = self.stft_pars['window'],
                n_fft = self.stft_pars['n_fft'])
        y_complex = y_complex[:, :correct_length]

        item['x_mag'] = np.abs(x_complex)
        item['x_phase'] = np.angle(x_complex)

        item['y_mag'] = np.abs(y_complex)
        item['y_phase'] = np.angle(y_complex)

        item['n_mag'] = np.abs(n_complex)
        item['n_phase'] = np.angle(n_complex)

        item['input1'] = np.real(y_complex)[:256]/self.spec_scaling
        item['input2'] = np.imag(y_complex)[:256]/self.spec_scaling

        item['output1'] = np.real(x_complex)[:256]/self.spec_scaling
        item['output2'] = np.imag(x_complex)[:256]/self.spec_scaling

        return item

    def get_data_sample(self, mode, data, sample_id):
        if mode == 'TEST':
            noise = self.test_noise
            noise_part = float(sample_id)/len(data)
            start = int(math.floor(noise_part*(noise.shape[0] - MAX_VALTEST_LENGTH*self.stft_pars['hop_length'])))
            snr = self.snr
        elif mode in ['VAL', 'TRAIN_DETERMINISTIC']:
            noise_sample = int(float(sample_id)/len(data)*len(self.train_noise))
            noise = self.train_noise[noise_sample]
            while noise.shape[0] < data[sample_id].shape[0]:
                noise_sample += 1
                if noise_sample >= len(self.train_noise):
                    noise_sample = 0
                noise = np.concatenate([noise, self.train_noise[noise_sample]])
            noise_part = float(sample_id)/len(data)
            start = int(math.floor(noise_part*(noise.shape[0] - data[sample_id].shape[0])))
            snr = self.snr
        elif mode == 'TRAIN_RANDOM':
            noise = np.array((), dtype=np.float32)
            while noise.shape[0] < data[sample_id].shape[0]:
                noise = np.concatenate([noise, self.train_noise[random.randint(0, self.train_noise.shape[0] - 1)]])
            start = random.randint(0, noise.shape[0] - data[sample_id].shape[0])
            snr = random.randint(-5, 0)
        else:
            raise NotImplementedError

        return self.get_data(data[sample_id].copy(), noise[start:start+data[sample_id].shape[0]], snr)

    def print_data_stats(self, mode):
        data = self.train_data if mode.startswith('TRAIN') else self.val_data if mode =='VAL' else self.test_data
        samples = [self.get_data_sample(mode, data, sid) \
                for sid in range(0, len(data), len(data)/INITIAL_STATS_SAMPLE_SIZE)]

        print "{} stats: ".format(mode)
        print "   num items: {}".format(len(data))

        min_lenght = 10000000000
        max_lenght = 0
        max_y = 0
        min_y = 100000
        sdr_original_y = 0.0
        sdr_perfect_x = 0.0
        for item in samples:
            min_lenght = min(min_lenght, item['input1'].shape[1])
            max_lenght = max(max_lenght, item['input1'].shape[1])

            max_y = max(max_y, item['input1'].max())
            min_y = min(min_y, item['input1'].min())

            sdr_original_y += testsdr(item['y_mag'], item['y_phase'], item['x_mag'], item['x_phase'])

            S_r = item['output1']
            S_i = item['output2']

            Y_r = item['input1']
            Y_i = item['input2']

            M_r = (Y_r*S_r + Y_i*S_i)/(Y_r**2.0 + Y_i**2.0+1e-5)
            M_i = (Y_r*S_i - Y_i*S_r)/(Y_r**2.0 + Y_i**2.0+1e-5)

            K = 10.0
            C = 0.1

            M_r = K * (1.0-np.exp(-C*M_r))/(1.0+np.exp(-C*M_r))
            M_i = K * (1.0-np.exp(-C*M_i))/(1.0+np.exp(-C*M_i))

            M_r = np.clip(M_r, -10.0 + 1e-6, 10.0 - 1e-6)
            M_i = np.clip(M_i, -10.0 + 1e-6, 10.0 - 1e-6)
            M_r = -(1.0/C)*np.log((K-M_r)/(K+M_r))
            M_i = -(1.0/C)*np.log((K-M_i)/(K+M_i))

            x_pred = (M_r*Y_r - M_i*Y_i) + 1j*(M_r*Y_i + M_i*Y_r)
            x_pred *= self.spec_scaling

            x_mag = item['y_mag'].copy()
            x_mag[:256] = np.abs(x_pred)

            x_pha = item['y_phase'].copy()
            x_pha[:256] = np.angle(x_pred)

            sdr_perfect_x += testsdr(x_mag, x_pha, item['x_mag'], item['x_phase'])

        print "   min max length: {} {}".format(min_lenght, max_lenght)
        print "   min max spectrogram value: {} {}".format(min_y, max_y)
        print "   average sdr of original y: {}".format(sdr_original_y/INITIAL_STATS_SAMPLE_SIZE)
        print "   average sdr if net out is perfect: {}".format(sdr_perfect_x/INITIAL_STATS_SAMPLE_SIZE)
        print ""

    def batch_creator(self, mode):
        data = self.train_data if mode.startswith('TRAIN') else self.val_data if mode =='VAL' else self.test_data
        batch_size = self.train_batch_size if mode.startswith('TRAIN') else 1
        train_length = MAX_VALTEST_LENGTH
        if mode.startswith('TRAIN'):
            random.shuffle(data)
            train_length = self.network.train_length()

        inputs = np.empty((batch_size, 2, self.network.num_freq(), train_length), dtype=np.float32)
        weights = np.empty((batch_size, 2, self.network.num_freq(), train_length), dtype=np.float32)
        targets = np.empty((batch_size, 2, self.network.num_freq(), train_length), dtype=np.float32)
        original_data = None
        
        sample_idx = 0
        while sample_idx + batch_size <= len(data):
            inputs.fill(0.0)
            weights.fill(0.0)
            targets.fill(0.0)
            for batch_idx in range(batch_size):
                sample = self.get_data_sample(mode, data, sample_idx + batch_idx)
                if sample['input1'].shape[1] > train_length:
                    start_idx_from = random.randint(0, sample['input1'].shape[1] - train_length)
                    stop_idx_from = start_idx_from + train_length
                    start_idx_to = 0
                    stop_idx_to = train_length
                else:
                    start_idx_from = 0
                    stop_idx_from = sample['input1'].shape[1]
                    start_idx_to = (train_length - sample['input1'].shape[1])/2
                    stop_idx_to = start_idx_to + sample['input1'].shape[1]
                assert(sample['input1'].shape[0] == self.network.num_freq())

                inputs[batch_idx, 0, :, start_idx_to:stop_idx_to] = sample['input1'][:, start_idx_from:stop_idx_from]
                inputs[batch_idx, 1, :, start_idx_to:stop_idx_to] = sample['input2'][:, start_idx_from:stop_idx_from]

                weights[batch_idx, :, :, start_idx_to:stop_idx_to] = 1.0

                S_r = sample['output1'][:, start_idx_from:stop_idx_from]
                S_i = sample['output2'][:, start_idx_from:stop_idx_from]

                Y_r = sample['input1'][:, start_idx_from:stop_idx_from]
                Y_i = sample['input2'][:, start_idx_from:stop_idx_from]

                M_r = (Y_r*S_r + Y_i*S_i)/(Y_r**2.0 + Y_i**2.0+1e-5)
                M_i = (Y_r*S_i - Y_i*S_r)/(Y_r**2.0 + Y_i**2.0+1e-5)

                K = 10.0
                C = 0.1

                M_r = K * (1.0-np.exp(-C*M_r))/(1.0+np.exp(-C*M_r))
                M_i = K * (1.0-np.exp(-C*M_i))/(1.0+np.exp(-C*M_i))

                targets[batch_idx, 0, :, start_idx_to:stop_idx_to] = M_r
                targets[batch_idx, 1, :, start_idx_to:stop_idx_to] = M_i

            sample_idx += batch_size
            if not mode.startswith('TRAIN'):
                original_data = (sample['x_mag'],
                                 sample['x_phase'],
                                 sample['y_mag'],
                                 sample['y_phase'],
                                 sample['n_mag'],
                                 sample['n_phase'], 0)
                yield inputs[:, :, :, start_idx_to:stop_idx_to], \
                      targets[:, :, :, start_idx_to:stop_idx_to], \
                      weights[:, :, :, start_idx_to:stop_idx_to], \
                      original_data
            else:
                yield inputs, targets, weights, None
