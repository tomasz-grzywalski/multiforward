#!/usr/bin/env python
import sys
import os
import time
import random
import numpy as np
import tensorflow as tf
import librosa
import cPickle as pickle
import yaml

from datetime import datetime

from dataset import Dataset
from networks.unet_dilated_v02 import Network
from evaluation.reco import testsdr


def fold(dataset, model_filename, net_output_number):
    input_spec_var = tf.placeholder(tf.float32, (None, 2, Network.num_freq(), None))
    bn_training = tf.placeholder(tf.bool, ())

    network = Network()
    network.create_layers()
    net_outputs = network.build(input_spec_var, bn_training)
    net_output = net_outputs[net_output_number - 1]

    sess = tf.Session()
    sys.stdout.flush()

    saver = tf.train.Saver()
    saver.restore(sess, model_filename)

    print("Starting testing...")
    sys.stdout.flush()

    test_sdr = 0.0
    test_batches = 0
    predictions = list()
    for batch in dataset.batch_creator(mode='TEST'):
        i, t, w, d = batch
        pred = sess.run(net_output, {input_spec_var : i,
                              bn_training : False})

        sdr, pred = calc_sdr(d, pred, i, dataset.get_spec_scaling())
        test_sdr += sdr
        predictions.append(pred)
        test_batches += 1

    print("   test      sdr:  \t\t{:.6f}".format(test_sdr / test_batches))
    sys.stdout.flush()
    return predictions

def calc_sdr(original_data, predictions, inputs, scaling):
    x_mag, x_phase, y_mag, y_phase, _, _, start_idx = original_data

    predictions = predictions[0, :, :, start_idx:start_idx+x_mag.shape[1]]
    predictions = np.clip(predictions, -10.0 + 1e-6, 10.0 - 1e-6)
    m_real_pred = predictions[0]
    m_imag_pred = predictions[1]

    inputs = inputs[0, :, :, start_idx:start_idx+x_mag.shape[1]]
    in_real = inputs[0]
    in_imag = inputs[1]

    K = 10.0
    C = 0.1

    m_real_pred = -(1.0/C)*np.log((K-m_real_pred)/(K+m_real_pred))
    m_imag_pred = -(1.0/C)*np.log((K-m_imag_pred)/(K+m_imag_pred))

    x_pred = (m_real_pred*in_real - m_imag_pred*in_imag) + 1j*(m_real_pred*in_imag + m_imag_pred*in_real)
    x_pred *= scaling

    x_mag_pred = y_mag.copy()
    x_mag_pred[:256] = np.abs(x_pred)

    x_phase_pred = y_phase.copy()
    x_phase_pred[:256] = np.angle(x_pred)

    return testsdr(x_mag_pred, x_phase_pred, x_mag, x_phase), (x_pred, original_data)

if __name__ == '__main__':
    random.seed(123)
    np.random.seed(123)
    tf.random.set_random_seed(123)

    if len(sys.argv) < 4:
        print "You must provide model filename, network output number, SNR and output filename"
        exit(0)
    net_output_number = int(sys.argv[2])
    assert net_output_number >= 1 and net_output_number <= 5
    snr = int(sys.argv[3])
    assert snr in [-5, 0, 5]

    paths = yaml.load(open('./properties.yaml', 'r'))

    dataset = Dataset(paths['data_path'], 1, Network, snr=snr, test_only=True, test_shopmall=False)
    predictions = fold(dataset, sys.argv[1], net_output_number)
    pickle.dump(predictions, open(sys.argv[4], "wb"))
