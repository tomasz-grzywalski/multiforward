#!/usr/bin/env python
import sys
import os
import time
import random
import numpy as np
import tensorflow as tf
import librosa
import cPickle as pickle
import yaml

from datetime import datetime

from dataset import Dataset
from networks.transformer import Network
from evaluation.reco import testsdr


def get_default_pars():
    pars = {'epochs' : 400,
            'batch_size' : 8,
            'learning_rate_decay' : 0.99,
            'speech_weight' : 0.75,
            'properties_file': './properties.yaml'}
    return pars

def fold(dataset, pars, out_path):
    num_epochs = pars['epochs']
    learning_rate_decay = pars['learning_rate_decay']
    speech_weight = pars['speech_weight']


    input_spec_var = tf.placeholder(tf.float32, (None, 2, Network.num_freq(), None))
    target_var = tf.placeholder(tf.float32, (None, 2, Network.num_freq(), None))
    weight_var = tf.placeholder(tf.float32, (None, 2, Network.num_freq(), None))
    learning_rate = tf.placeholder(tf.float32, ())
    bn_training = tf.placeholder(tf.bool, ())

    learning_rate_val = Network.learning_rate()

    network = Network()
    network.create_layers()
    net1, net2, net3, net4, net5 = network.build(input_spec_var, bn_training)

    train_loss1 = tf.reduce_mean((net1 - target_var)**2.0, axis=2, keepdims=True)*weight_var
    train_loss1 = tf.reduce_mean(train_loss1)
    train_loss = train_loss1

    test_loss1 = tf.reduce_mean((net1 - target_var)**2.0, axis=2, keepdims=True)*weight_var
    test_loss1 = tf.reduce_mean(test_loss1)

    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    update = optimizer.minimize(loss=train_loss)
    extra_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    all_variables = tf.trainable_variables()

    n_pars = np.sum([np.prod(v.get_shape().as_list()) for v in tf.trainable_variables()])
    print "Total number of network's parameters: {}".format(n_pars)
    sys.stdout.flush()

    saver = tf.train.Saver(max_to_keep=None)
    model_filename = os.path.join(out_path, "model.ckpt")

    print("Starting training...")
    sys.stdout.flush()
    best_val_sdr = 0.0
    prev_train_err = 1000000.0
    # We iterate over epochs:
    for epoch in range(num_epochs):
        # In each epoch, we do a full pass over the training data:
        train1_err = 0
        train_batches = 0
        start_time = time.time()
        #backup = lasagne.layers.get_all_param_values(net)
        backup = [sess.run(n).copy() for n in all_variables]
        
        for batch in dataset.batch_creator(mode='TRAIN_RANDOM'):
            i, t, w, _ = batch
            _, _, err1 = sess.run(
                    [update, extra_update_ops, train_loss1],
                    {input_spec_var : i,
                        target_var : t,
                        weight_var : w,
                        learning_rate: learning_rate_val,
                        bn_training : True})

            train1_err += err1

            train_batches += 1
        if train1_err / train_batches > 1.1 * prev_train_err:
            # Then we print the results for this epoch:
            print("Epoch {} of {} took {:.3f}s".format(
                epoch + 1, num_epochs, time.time() - start_time))
            print("   training1 loss:  \t\t{:.6f}".format(train1_err / train_batches))
            print("   catastophic event - restoring previous weights")
            #lasagne.layers.set_all_param_values(net, backup)
            for var, val in zip(all_variables, backup):
                sess.run(var.assign(val))
            learning_rate_val *= learning_rate_decay            
            continue
        else:
           prev_train_err = train1_err / train_batches
        #learning_rate.set_value(lasagne.utils.floatX(learning_rate.get_value() * learning_rate_decay))
        learning_rate_val *= learning_rate_decay
        # And a full pass over the validation data:
        val1_err = 0
        val_batches = 0
        val1_sdr = 0.0
        for batch in dataset.batch_creator(mode='VAL'):
            i, t, w, d = batch
            pred1, err1 = sess.run(
                    [net1, test_loss1],
                    {input_spec_var : i,
                        target_var : t,
                        weight_var : w,
                        bn_training : False})

            val1_err += err1
            sdr1, _ = calc_sdr(d, pred1, i, dataset.get_spec_scaling())
            val1_sdr += sdr1

            val_batches += 1

        # Then we print the results for this epoch:
        print("Epoch {} of {} took {:.3f}s".format(
            epoch + 1, num_epochs, time.time() - start_time))
        print("   training1 loss:  \t\t{:.6f}".format(train1_err / train_batches))
        print("   val1      loss:  \t\t{:.6f}".format(val1_err / val_batches))
        print("   val1       sdr:  \t\t{:.6f}".format(val1_sdr / val_batches))

        if val1_sdr / val_batches > best_val_sdr:
            print "   new best val sdr!"
            best_val_sdr = val1_sdr / val_batches

            saver.save(sess, model_filename)

            test1_sdr = 0.0
            test_batches = 0
            predictions = list()
            for batch in dataset.batch_creator(mode='TEST'):
                i, t, w, d = batch
                pred1 = sess.run(
                        [net1],
                        {input_spec_var : i,
                             target_var : t,
                             weight_var : w,
                             bn_training : False})[0]

                sdr1, pred = calc_sdr(d, pred1, i, dataset.get_spec_scaling())
                test1_sdr += sdr1

                predictions.append(pred)
                test_batches += 1

            print("   test1      sdr:  \t\t{:.6f}".format(test1_sdr / test_batches))
        sys.stdout.flush()
    return predictions

def calc_sdr(original_data, predictions, inputs, scaling):
    x_mag, x_phase, y_mag, y_phase, _, _, start_idx = original_data

    predictions = predictions[0, :, :, start_idx:start_idx+x_mag.shape[1]]
    predictions = np.clip(predictions, -10.0 + 1e-6, 10.0 - 1e-6)
    m_real_pred = predictions[0]
    m_imag_pred = predictions[1]

    inputs = inputs[0, :, :, start_idx:start_idx+x_mag.shape[1]]
    in_real = inputs[0]
    in_imag = inputs[1]

    K = 10.0
    C = 0.1

    m_real_pred = -(1.0/C)*np.log((K-m_real_pred)/(K+m_real_pred))
    m_imag_pred = -(1.0/C)*np.log((K-m_imag_pred)/(K+m_imag_pred))

    x_pred = (m_real_pred*in_real - m_imag_pred*in_imag) + 1j*(m_real_pred*in_imag + m_imag_pred*in_real)
    x_pred *= scaling

    x_mag_pred = y_mag.copy()
    x_mag_pred[:256] = np.abs(x_pred)

    x_phase_pred = y_phase.copy()
    x_phase_pred[:256] = np.angle(x_pred)

    return testsdr(x_mag_pred, x_phase_pred, x_mag, x_phase), (x_pred, original_data)

if __name__ == '__main__':
    random.seed(123)
    np.random.seed(123)
    tf.random.set_random_seed(123)

    if len(sys.argv) == 1:
        pars = get_default_pars()
    elif len(sys.argv) == 2:
        pars = yaml.load(open(sys.argv[1], 'r'))

    paths = yaml.load(open(pars['properties_file'], 'r'))


    datetime_tag = datetime.now().isoformat()
    out_dir = os.path.join(paths['output_path'], datetime_tag)
    os.makedirs(out_dir)

    open('./exp.tag', 'wt').write(datetime_tag)
        
    OUTPUT_FILE = os.path.join(out_dir, "x_pred.pkl")

    batch_size = pars['batch_size']

    dataset = Dataset(paths['data_path'], batch_size, Network)
    predictions = fold(dataset, pars, out_dir)
    pickle.dump(predictions, open(OUTPUT_FILE, "wb"))
