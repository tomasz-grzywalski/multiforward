class NetworkBase:
    @staticmethod
    def build(variables, num_channels, num_outputs):
        raise NotImplementedError

    @staticmethod
    def scale():
        raise NotImplementedError

    @staticmethod
    def train_length():
        raise NotImplementedError

    @staticmethod
    def train_margin():
        raise NotImplementedError

    @staticmethod
    def train_speclen():
        raise NotImplementedError

    @staticmethod
    def ws_inputs():
        raise NotImplementedError

    @staticmethod
    def momentum():
        raise NotImplementedError

    @staticmethod
    def learning_rates():
        raise NotImplementedError
