import numpy as np
import tensorflow as tf

from base import NetworkBase

class Network(NetworkBase):
    def __init__(self):
        self.NUM_FLT = 512

    @staticmethod
    def create_resLSTM(num_units):
        lstm_layer = tf.keras.layers.CuDNNLSTM(units=num_units, return_sequences=True)
        bidirectional = tf.keras.layers.Bidirectional(lstm_layer, merge_mode='concat')

        return bidirectional

    @staticmethod
    def create_conv(filters, kernel, strides):
        conv = tf.keras.layers.Conv1D(filters, kernel, strides, padding='SAME', activation=None)

        return conv

    def create_layers(self):
        self.input_convc = Network.create_conv(self.NUM_FLT, 1, 1)

        self.lstm1 = Network.create_resLSTM(self.NUM_FLT/2)
        self.lstm2 = Network.create_resLSTM(self.NUM_FLT/2)
        self.lstm3 = Network.create_resLSTM(self.NUM_FLT/2)
        self.pre_out_conv = Network.create_conv(self.NUM_FLT, 1, 1)

        self.output_conv = Network.create_conv(Network.num_freq()*2, 1, 1)

    @staticmethod
    def train_length():
        return 500

    @staticmethod
    def num_freq():
        return 256

    @staticmethod
    def learning_rate():
        return 0.002

    def build(self, input_var, bn_training):
        batch_size = tf.shape(input_var)[0]
        num_channels = tf.shape(input_var)[1]
        num_freq = tf.shape(input_var)[2]
        n_frames = tf.shape(input_var)[3]

        input_var = tf.transpose(input_var, (0, 3, 1, 2))
        input_var = tf.reshape(input_var, (batch_size, n_frames, Network.num_freq()*2))
        #batch x num_freq x time x channels

        network = self.input_convc(input_var)
        network = tf.layers.batch_normalization(inputs=network, training=bn_training, momentum=0.9, epsilon=1e-4)
        lstm_in = tf.nn.elu(network)

        features1 = self.build_lstm(lstm_in, bn_training)
        output1 = self.build_output(features1, batch_size, n_frames, num_channels, num_freq)

        features2 = self.build_lstm(lstm_in + features1, bn_training)
        output2 = self.build_output(features2, batch_size, n_frames, num_channels, num_freq)

        features3 = self.build_lstm(lstm_in + features2, bn_training)
        output3 = self.build_output(features3, batch_size, n_frames, num_channels, num_freq)

        features4 = self.build_lstm(lstm_in + features3, bn_training)
        output4 = self.build_output(features4, batch_size, n_frames, num_channels, num_freq)

        features5 = self.build_lstm(lstm_in + features4, bn_training)
        output5 = self.build_output(features5, batch_size, n_frames, num_channels, num_freq)

        return output1, output2, output3, output4, output5

    def build_lstm(self, network, bn_training):
        network += self.lstm1(network)
        network += self.lstm2(network)
        network += self.lstm3(network)
        network = self.pre_out_conv(network)
        network = tf.layers.batch_normalization(inputs=network, training=bn_training, momentum=0.9, epsilon=1e-4)
        network = tf.nn.elu(network)

        return network

    def build_output(self, network, batch_size, n_frames, num_channels, num_freq):
        network = self.output_conv(network)
        network = tf.reshape(network, (batch_size, n_frames, num_channels, num_freq))
        network = tf.transpose(network, (0, 2, 3, 1))

        return network
