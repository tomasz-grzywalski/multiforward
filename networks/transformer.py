import numpy as np
import tensorflow as tf

from base import NetworkBase


class MultiHeadAttention(tf.keras.layers.Layer):
    def __init__(self, d_model, num_heads):
        super(MultiHeadAttention, self).__init__()
        self.num_heads = num_heads
        self.d_model = d_model

        assert d_model % self.num_heads == 0

        self.depth = d_model // self.num_heads

        self.wq = tf.keras.layers.Dense(d_model)
        self.wk = tf.keras.layers.Dense(d_model)
        self.wv = tf.keras.layers.Dense(d_model)

        self.dense = tf.keras.layers.Dense(d_model)

    def split_heads(self, x, batch_size):
        """Split the last dimension into (num_heads, depth).
        Transpose the result such that the shape is (batch_size, num_heads, seq_len, depth)
        """
        x = tf.reshape(x, (batch_size, -1, self.num_heads, self.depth))
        return tf.transpose(x, perm=[0, 2, 1, 3])

    def call(self, v, k, q, mask):
        batch_size = tf.shape(q)[0]

        q = self.wq(q)  # (batch_size, seq_len, d_model)
        k = self.wk(k)  # (batch_size, seq_len, d_model)
        v = self.wv(v)  # (batch_size, seq_len, d_model)

        q = self.split_heads(q, batch_size)  # (batch_size, num_heads, seq_len_q, depth)
        k = self.split_heads(k, batch_size)  # (batch_size, num_heads, seq_len_k, depth)
        v = self.split_heads(v, batch_size)  # (batch_size, num_heads, seq_len_v, depth)

        # scaled_attention.shape == (batch_size, num_heads, seq_len_q, depth)
        # attention_weights.shape == (batch_size, num_heads, seq_len_q, seq_len_k)
        scaled_attention, attention_weights = scaled_dot_product_attention(
            q, k, v, mask)

        scaled_attention = tf.transpose(scaled_attention, perm=[0, 2, 1, 3])  # (batch_size, seq_len_q, num_heads, depth)

        concat_attention = tf.reshape(scaled_attention,
                                      (batch_size, -1, self.d_model))  # (batch_size, seq_len_q, d_model)

        output = self.dense(concat_attention)  # (batch_size, seq_len_q, d_model)

        return output, attention_weights


def scaled_dot_product_attention(q, k, v, mask):
    """Calculate the attention weights.
    q, k, v must have matching leading dimensions.
    k, v must have matching penultimate dimension, i.e.: seq_len_k = seq_len_v.
    The mask has different shapes depending on its type(padding or look ahead)
    but it must be broadcastable for addition.

    Args:
      q: query shape == (..., seq_len_q, depth)
      k: key shape == (..., seq_len_k, depth)
      v: value shape == (..., seq_len_v, depth_v)
      mask: Float tensor with shape broadcastable
            to (..., seq_len_q, seq_len_k). Defaults to None.

    Returns:
      output, attention_weights
    """

    matmul_qk = tf.matmul(q, k, transpose_b=True)  # (..., seq_len_q, seq_len_k)

    # scale matmul_qk
    dk = tf.cast(tf.shape(k)[-1], tf.float32)
    scaled_attention_logits = matmul_qk / tf.math.sqrt(dk)

    # add the mask to the scaled tensor.
    if mask is not None:
        scaled_attention_logits += (mask * -1e9)

    # softmax is normalized on the last axis (seq_len_k) so that the scores
    # add up to 1.
    attention_weights = tf.nn.softmax(scaled_attention_logits, axis=-1)  # (..., seq_len_q, seq_len_k)

    output = tf.matmul(attention_weights, v)  # (..., seq_len_q, depth_v)

    return output, attention_weights



def point_wise_feed_forward_network(d_model, dff):
    return tf.keras.Sequential([
        tf.keras.layers.Dense(dff, activation='elu'),  # (batch_size, seq_len, dff)
        tf.keras.layers.Dense(d_model, activation='elu')  # (batch_size, seq_len, d_model)
    ])


class EncoderLayer(tf.keras.layers.Layer):
    def __init__(self, d_model, num_heads, dff, rate=0.1):
        # d_model = 320 / 160 (num_outputs * scale * 8/4)
        super(EncoderLayer, self).__init__()

        self.mha = MultiHeadAttention(d_model, num_heads)
        self.ffn = point_wise_feed_forward_network(d_model, dff)

    def call(self, x, training, mask):
        attn_output, _ = self.mha(x, x, x, mask)  # (batch_size, input_seq_len, d_model)
        out1 = x + attn_output

        ffn_output = self.ffn(out1)  # (batch_size, input_seq_len, d_model)
        out2 = out1 + ffn_output

        return out2


class Encoder(tf.keras.layers.Layer):
    def __init__(self, num_layers, d_model, num_heads, dff, rate=0.1):
        super(Encoder, self).__init__()

        self.d_model = d_model
        self.num_layers = num_layers

        self.enc_layers = [EncoderLayer(d_model, num_heads, dff, rate) for _ in range(num_layers)]

    def call(self, x, training, mask):
        seq_len = tf.shape(x)[1]

        for i in range(self.num_layers):
            x = self.enc_layers[i](x, training=training, mask=mask)

        return x  # (batch_size, input_seq_len, d_model)



class Network(NetworkBase):
    def __init__(self):
        self.NUM_C = 60

    @staticmethod
    def create_conv(transposed, filters, kernel, strides):
        conv = tf.keras.layers.Conv2DTranspose if transposed else tf.keras.layers.Conv2D
        conv = conv(filters, kernel, strides, padding='SAME', activation=None)

        return conv

    def create_layers(self):
        self.input_conv = Network.create_conv(False, self.NUM_C, (3, 3), (2, 2))

        self.in1_conv = Network.create_conv(False, self.NUM_C, (3, 3), (1, 1))
        self.in2_conv = Network.create_conv(False, self.NUM_C, (3, 3), (2, 1))
        self.in3_conv = Network.create_conv(False, self.NUM_C, (3, 3), (1, 1))

        self.transform_freq = Encoder(num_layers=1, d_model=self.NUM_C, num_heads=4, dff=self.NUM_C)
        self.transform_time = Encoder(num_layers=1, d_model=self.NUM_C, num_heads=4, dff=self.NUM_C)

        self.out1_conv = Network.create_conv(True, self.NUM_C, (6, 3), (2, 1))

        self.output_conv = tf.keras.layers.Conv2DTranspose(2, (6, 6), (2, 2), padding='SAME', activation=None)

    @staticmethod
    def train_length():
        return 500

    @staticmethod
    def num_freq():
        return 256

    @staticmethod
    def learning_rate():
        return 0.002

    def build(self, input_var, bn_training):
        n_frames = tf.shape(input_var)[3]
        input_var = tf.transpose(input_var, (0, 2, 3, 1))
        #batch x num_freq x time x channels

        network = self.input_conv(input_var)
        network = tf.layers.batch_normalization(inputs=network, training=bn_training, momentum=0.9, epsilon=1e-4)
        unet_in = tf.nn.elu(network)

        features1 = self.build_trans(unet_in, bn_training)

        network = self.output_conv(features1)
        output1 = tf.transpose(network, (0, 3, 1, 2))


        features2 = self.build_trans(unet_in + features1, bn_training)

        network = self.output_conv(features2)
        output2 = tf.transpose(network, (0, 3, 1, 2))


        features3 = self.build_trans(unet_in + features2, bn_training)

        network = self.output_conv(features3)
        output3 = tf.transpose(network, (0, 3, 1, 2))


        features4 = self.build_trans(unet_in + features3, bn_training)

        network = self.output_conv(features4)
        output4 = tf.transpose(network, (0, 3, 1, 2))


        features5 = self.build_trans(unet_in + features4, bn_training)

        network = self.output_conv(features5)
        output5 = tf.transpose(network, (0, 3, 1, 2))


        return output1, output2, output3, output4, output5

    def build_trans(self, network, bn_training):
        network = self.in1_conv(network)
        network = tf.layers.batch_normalization(inputs=network, training=bn_training, momentum=0.9, epsilon=1e-4)
        network = tf.nn.elu(network)

        network = self.in2_conv(network)
        network = tf.layers.batch_normalization(inputs=network, training=bn_training, momentum=0.9, epsilon=1e-4)
        network = skip = tf.nn.elu(network)

        network = self.in3_conv(network)
        network = tf.layers.batch_normalization(inputs=network, training=bn_training, momentum=0.9, epsilon=1e-4)
        network = tf.nn.elu(network)

       
        num_batches = tf.shape(network)[0]
        num_freq = tf.shape(network)[1]
        time_steps = tf.shape(network)[2]
        num_channels = tf.shape(network)[3]


        network = tf.transpose(network, (0, 2, 1, 3))
        network = tf.reshape(network, (-1, num_freq, self.NUM_C))

        network = self.transform_freq(network, training=bn_training, mask=None)

        network = tf.reshape(network, (-1, time_steps, num_freq, self.NUM_C))
        network = tf.transpose(network, (0, 2, 1, 3))


        network = tf.reshape(network, (-1, time_steps, self.NUM_C))

        network = self.transform_time(network, training=bn_training, mask=None)

        network = tf.reshape(network, (-1, num_freq, time_steps, self.NUM_C))


        network = self.out1_conv(network + skip)
        network = tf.layers.batch_normalization(inputs=network, training=bn_training, momentum=0.9, epsilon=1e-4)
        network = tf.nn.elu(network)
        return network
