import numpy as np
import tensorflow as tf

from base import NetworkBase

data_format='channels_last'

class Network(NetworkBase):
    def __init__(self):
        self.NUM_C = 54
        self.NUM_D = 16

    @staticmethod
    def create_conv(transposed, filters, kernel, strides):
        conv = tf.keras.layers.Conv2DTranspose if transposed else tf.keras.layers.Conv2D
        conv = conv(filters, kernel, strides, padding='SAME', activation=None)

        return [conv]

    @staticmethod
    def create_dilated(filters, kernel, dilation_rate):
        conv = tf.keras.layers.Conv2D(filters, kernel, dilation_rate=dilation_rate, padding='SAME', activation=None)

        return [conv]

    @staticmethod
    def create_rt(filters):
        rt = list()
        rt.append(Network.create_dilated(filters/2, (1, 7), (1, 3)))
        rt.append(Network.create_dilated(filters/2, (1, 7), (1, 4)))

        return rt

    @staticmethod
    def create_rf(filters):
        rf = list()
        rf.append(Network.create_dilated(filters/2, (7, 1), (2, 1)))
        rf.append(Network.create_dilated(filters/2, (7, 1), (3, 1)))

        return rf

    def create_layers(self):
        self.wc_ic = Network.create_conv(False, self.NUM_C, (3, 3), (2, 2))

        self.wc_1c_rt = Network.create_rt(self.NUM_D)
        self.wc_1c_c = Network.create_conv(False, self.NUM_C, (3, 3), (1, 1))

        self.wc_2c_rf = Network.create_rf(self.NUM_D)
        self.wc_2c_c = Network.create_conv(False, self.NUM_C, (3, 3), (2, 1))

        self.wc_3c_rt = Network.create_rt(self.NUM_D)
        self.wc_3c_c = Network.create_conv(False, self.NUM_C, (3, 3), (1, 1))

        self.wc_3d_rf = Network.create_rf(self.NUM_D)
        self.wc_3d_c = Network.create_conv(False, self.NUM_C, (3, 3), (1, 1))

        self.wc_2d_rt = Network.create_rt(self.NUM_D)
        self.wc_2d_c = Network.create_conv(True, self.NUM_C, (6, 3), (2, 1))

        self.wc_1d_rf = Network.create_rf(self.NUM_D)
        self.wc_1d_c = Network.create_conv(False, self.NUM_C, (3, 3), (1, 1))

        self.wc_oc = tf.keras.layers.Conv2DTranspose(2, (6, 6), (2, 2), padding='SAME', activation=None)

    @staticmethod
    def train_length():
        return 500

    @staticmethod
    def num_freq():
        return 256

    @staticmethod
    def learning_rate():
        return 0.002

    def build(self, input_var, bn_training):
        n_frames = tf.shape(input_var)[3]
        input_var = tf.transpose(input_var, (0, 2, 3, 1))
        #batch x num_freq x time x channels

        network = self.wc_ic[0](input_var)
        net_lvl1 = network
        network = tf.layers.batch_normalization(inputs=network, training=bn_training, momentum=0.9, epsilon=1e-4)
        unet_in = tf.nn.elu(network)

        network = self.build_unet(unet_in, bn_training)
        network = network + net_lvl1
        network = tf.layers.batch_normalization(inputs=network, training=bn_training, momentum=0.9, epsilon=1e-4)
        network = features1 = tf.nn.elu(network)

        network = self.wc_oc(network)
        output1 = tf.transpose(network, (0, 3, 1, 2))


        network = self.build_unet(unet_in + features1, bn_training)
        network = network + net_lvl1
        network = tf.layers.batch_normalization(inputs=network, training=bn_training, momentum=0.9, epsilon=1e-4)
        network = features2 = tf.nn.elu(network)

        network = self.wc_oc(network)
        output2 = tf.transpose(network, (0, 3, 1, 2))


        network = self.build_unet(unet_in + features2, bn_training)
        network = network + net_lvl1
        network = tf.layers.batch_normalization(inputs=network, training=bn_training, momentum=0.9, epsilon=1e-4)
        network = features3 = tf.nn.elu(network)

        network = self.wc_oc(network)
        output3 = tf.transpose(network, (0, 3, 1, 2))


        network = self.build_unet(unet_in + features3, bn_training)
        network = network + net_lvl1
        network = tf.layers.batch_normalization(inputs=network, training=bn_training, momentum=0.9, epsilon=1e-4)
        network = features4 = tf.nn.elu(network)

        network = self.wc_oc(network)
        output4 = tf.transpose(network, (0, 3, 1, 2))


        network = self.build_unet(unet_in + features4, bn_training)
        network = network + net_lvl1
        network = tf.layers.batch_normalization(inputs=network, training=bn_training, momentum=0.9, epsilon=1e-4)
        network = tf.nn.elu(network)

        network = self.wc_oc(network)
        output5 = tf.transpose(network, (0, 3, 1, 2))


        return output1, output2, output3, output4, output5

    def build_unet(self, network, bn_training):
        network = Network.build_gated_wcu(self.wc_1c_rt, network, bn_training)
        network = self.wc_1c_c[0](network)

        net_lvl2 = network
        network = tf.layers.batch_normalization(inputs=network, training=bn_training, momentum=0.9, epsilon=1e-4)
        network = tf.nn.elu(network)


        network = Network.build_gated_wcu(self.wc_2c_rf, network, bn_training)
        network = self.wc_2c_c[0](network)

        net_lvl3 = network
        network = tf.layers.batch_normalization(inputs=network, training=bn_training, momentum=0.9, epsilon=1e-4)
        network = tf.nn.elu(network)


        network = Network.build_gated_wcu(self.wc_3c_rt, network, bn_training)
        network = self.wc_3c_c[0](network)
        network = tf.layers.batch_normalization(inputs=network, training=bn_training, momentum=0.9, epsilon=1e-4)
        network = tf.nn.elu(network)


        network = Network.build_gated_wcu(self.wc_3d_rf, network, bn_training)
        network = self.wc_3d_c[0](network)

        network = network + net_lvl3
        network = tf.layers.batch_normalization(inputs=network, training=bn_training, momentum=0.9, epsilon=1e-4)
        network = tf.nn.elu(network)


        network = Network.build_gated_wcu(self.wc_2d_rt, network, bn_training)
        network = self.wc_2d_c[0](network)

        network = network + net_lvl2
        network = tf.layers.batch_normalization(inputs=network, training=bn_training, momentum=0.9, epsilon=1e-4)
        network = tf.nn.elu(network)


        network = Network.build_gated_wcu(self.wc_1d_rf, network, bn_training)
        network = self.wc_1d_c[0](network)

        return network

    @staticmethod
    def build_gated_wcu(wcu, network, bn_training):
        net1 = wcu[0][0](network)
        net1 = tf.layers.batch_normalization(inputs=net1, training=bn_training, momentum=0.9, epsilon=1e-4)


        net2 = wcu[1][0](network)
        net2 = tf.layers.batch_normalization(inputs=net2, training=bn_training, momentum=0.9, epsilon=1e-4)

        return tf.concat([network, net1, net2], axis=-1)
