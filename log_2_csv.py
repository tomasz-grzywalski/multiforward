import sys

def read_vals(filename):
    lines = open(filename, "rt").readlines()
    sdr = float(lines[-6].split(' ')[-1][:-2])
    stoi = float(lines[-2].split(' ')[-1][:-2])
    pesq = float(lines[-1].split(' ')[-1][:-2])
    return sdr, stoi, pesq

def print_vals(vals):
    print(f"{vals[0]},{vals[1]},{vals[2]},{vals[3]},{vals[4]},{vals[5]},{vals[6]},{vals[7]},{vals[8]}")

line = [0.0]*9
if __name__ == '__main__':
    assert len(sys.argv) == 19
    for idx in range(0, 18):
        pos = idx % 3
        line[pos], line[pos + 3], line[pos + 6] = read_vals(sys.argv[idx + 1])
        if pos == 2:
            print_vals(line)
